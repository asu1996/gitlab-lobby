Tor's Gitlab Lobby
==================

This Django application contains the lobby website for Tor's Gitlab instance.
The Gitlab Lobby allows users to:

1. Request accounts on our Gitlab server, if they are interested in working
   with Tor's development teams.

2. Anonymously submit and comment on issues on Tor's Gitlab instance.

We currently run a trial instance of this project on
<https://gitlab.onionize.space>. Users can request Gitlab accounts from there.

# To run it locally

You need to start by setting the `SECRET_KEY` variable in
`src/lobby/settings.py`. This value can be an arbitrary string, and you should
never commit changes to `settings.py`.

Then run the following commands:

```
$ virtualenv -p python3.7 .env
$ source .env/bin/activate
$ pip install -r requirements.txt
$ python src/manage.py makemigrations
$ python src/manage.py migrate
$ python src/manage.py runserver
```

When `runserver` is running, you should be able to point your browser to
<http://127.0.0.1:8000/> and reach the local version of the application.
