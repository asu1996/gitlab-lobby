from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import GitlabAccountRequestForm
from .gitlab import approve_user

from lobby.settings import AUTO_ACCEPT_LIST

def index(request):
    if request.method == "POST":
        form = GitlabAccountRequestForm(request.POST)

        if form.is_valid():
            model = form.save()

            # Auto-accept some accounts (if the username is free).
            email = model.email

            for hostname in AUTO_ACCEPT_LIST:
                email = email.lower()

                if email.endswith("@" + hostname):
                    approve_user(model)

            return HttpResponseRedirect('/success/')

        return HttpResponseRedirect('/failure/')

    return render(request, 'core/signup.html')

def success(request):
    return render(request, 'core/success.html')

def failure(request):
    return render(request, 'core/failure.html')
