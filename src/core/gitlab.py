import gitlab

from lobby.settings import GITLAB_URL, GITLAB_SECRET_TOKEN

def approve_user(model):
    client = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_SECRET_TOKEN)

    try:
        user = client.users.create({
            "name":              model.username,
            "username":          model.username.replace(" ", "_"),
            "email":             model.email,
            "reset_password":    True,
            "can_create_group":  False,
            "skip_confirmation": True, # The password reset mail is enough.
        })

        user.projects_limit = 5
        user.save()
    except Exception as e:
        print("Error: {}".format(e))
        return

    model.approved = True
    model.save()
